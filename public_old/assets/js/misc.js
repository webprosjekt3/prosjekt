$(function() {
     $('button[type="submit"]').click(function() {
          $(this).button('loading');
     });

     $('table').on('click', 'tr.linkable', function(e) {
          if($(e.target).closest('a, button, input').length) return;
          location.href = $(this).data('url');
     });
     $('.alert').alert();
});





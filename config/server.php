<?php
     define('VHOST_PATH', '/etc/apache2/sites-available'); // Path to server VHost file 
     define('VHOST_PORT', '80'); // Port for VHost (Needs to be defined in ports.conf)
     define('APACHE_EXEC', '/etc/init.d/apache2'); // Path to apache2 executable 
     define('A2ENSITE_EXEC', '/usr/sbin/a2ensite'); // Path to a2ensite executable 
     define('A2DISSITE_EXEC', '/usr/sbin/a2dissite'); // Path to a2dissite executable 
?>

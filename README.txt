Setup:

     1. Please configure your own values in config/*.php files
     2. Run composer install to install required frameworks


Additionally:

     1. Make sure your server can use rules defined in .htaccess
     2. Make sure your apache user (default www-data) has access rights to VHost files (read / write)
          and to sites/* folders
     3. Github repo needs to be setup with SSH-keys from the server, in order to authenticate.

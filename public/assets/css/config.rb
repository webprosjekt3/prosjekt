preferred_syntax = :scss
http_path = '/'
css_dir = './'
sass_dir = 'scss/'
images_dir = '../img/'
javascripts_dir = '../js/'
relative_assets = true
line_comments = false
require 'bootstrap-sass'
output_style = :compressed

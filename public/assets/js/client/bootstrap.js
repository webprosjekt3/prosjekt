var router = new Router()
.addRoute('#/', function(req, next) {
     function getSites() {
          return $.get('http://84.48.125.17:8080/skole/gruppe/web3/api/sites').pipe(function(promise) {
               return promise.sites;
          });
     }
     Handlebars.getTemplate('index/index').done(function(template) {
          $.when(getSites()).done(function(sites) {
               $('main').html(template({sites:sites}));
          });
     });
})
.addRoute('#/site/new', function(req, next) {
     Handlebars.getTemplate('site/new').done(function(template) {
          $('main').html(template());
     });
})
.addRoute('#/site/:id/edit', function(req, next) {
     function getSite(id) {
          return $.get('http://84.48.125.17:8080/skole/gruppe/web3/api/site/' + id).pipe(function(promise) {
               return promise.site;
          });
     }
     Handlebars.getTemplate('site/edit').done(function(template) {
          $.when(getSite(req.params.id)).done(function(site) {
               $('main').html(template({site: site}));
          });
     });
})
.addRoute('#/site/:id', function(req, next) {
     function getSite(id) {
          return $.get('http://84.48.125.17:8080/skole/gruppe/web3/api/site/' + id).pipe(function(promise) {
               return promise.site;
          });
     }
     function getFiles(id) {
          return $.get('http://84.48.125.17:8080/skole/gruppe/web3/api/site/' + id + '/files').pipe(function(promise) {
               return promise.files;
          });
     }
     function getCommits(id) {
           return $.get('http://84.48.125.17:8080/skole/gruppe/web3/api/site/'+ id + '/git/commits').pipe(function(promise) {
               return promise.commits;
           });
     }
     Handlebars.getTemplate('site/view').done(function(template) {
          $.when(getSite(req.params.id), getFiles(req.params.id), getCommits(req.params.id)).done(function(site, files, commits) {
               $('main').html(template({site: site, files: files, commits: commits}));
          });
     });
})
.run('#/');

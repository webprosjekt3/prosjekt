Handlebars.getTemplate = function(name) {
	return $.getScript('public/assets/js/misc.js').then(function() {
     	return $.get('public/views/' + name + '.html').then(function(template) {
          	return Handlebars.compile(template);
          });
     });
};

Handlebars.registerHelper('if_eq', function(a, b, opts) {
    if(a == b)
        return opts.fn(this);
    else
        return opts.inverse(this);
});

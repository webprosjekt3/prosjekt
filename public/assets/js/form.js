$(function() {
	$('form').submit(function(e) {
  		e.preventDefault();
    		$.ajax({
      		url: $(this).attr('action'),
      		method: $(this).attr('method'),
      		data: $(this).serialize(),
      		success: function(data) {
          		alert('Successfully updated!');
          		window.location.replace("#/");
      		}, error: function(data) {
                    var res = JSON.parse(data.responseText);
          		for(error in res.errors) {
  					alert('Error on: ' + error + '\n' + res.errors[error]);
				}
				window.history.go(-1);;
      		}, beforeSend: function(xhr, settings) {
        			xhr.setRequestHeader('X-Requested-With','XMLHttpRequest'); 
    			}
  		});
	});
});

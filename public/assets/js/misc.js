$(function() {
	$('main').html();
	$(document).bind("ajaxStart.loader", function() {
		$('main').html('<div class="loader"></div>');
	});
    	$('button').click(function() {
		$(document).unbind(".loader");
    		$.get('http://172.17.81.185:8888/prosjekt/api/server/' + $(this).data('action'));
	});
     $('button[type="submit"]').click(function() {
          $(this).button('loading');
     });
     $('table').on('click', 'tr.linkable', function(e) {
          if($(e.target).closest('a, button, input').length) return;
          location.href = $(this).data('url');
     });
     $('.alert').alert();
});
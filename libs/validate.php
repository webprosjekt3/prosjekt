<?php
/**
 * Validates values against certain datatypes using regex
 */
class Validate {

     function __construct() {
     }

     /**
     * Sets specifications for validation types
     *
     * @static
     */
     public static $check = array(
          'letters'  => "/^([a-zA-Z]+\s)*[a-zA-Z]+$/",
          'digit'    => "/^\d+$/",
          'password' => "/^.{6,16}$/",
          'email'    => "/^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/",
          'domain'   => "/^(?!www)[A-Za-z0-9-]+\.[A-z]+$/",
          'image'    => array(
                              'types' => array('image/jpg','image/jpeg','image/png'),
                              'size'  => '5120' // 1 MB
                            )
     );
     
     /**
     * Strings
     *
     * @param  string    $key           Field name
     * @param  string    $value         Field value
     * @param  bool      $required      If field is required
     * @return                          void or error message
     */
     public function string($value, $required) {
          if(empty($value) && $required) {
               return 'Please fill in';
          }
     }
     
     /**
     * Letters
     *
     * @param  string    $key           Field name
     * @param  string    $value         Field value
     * @param  bool      $required      If field is required
     * @return                          void or error message
     */
     public function letters($value, $required) {
          if(empty($value) && $required) {
               return 'Please fill in';
          }
          elseif(!preg_match(self::$check['letters'], $value) && !empty($value)) {
               return 'Only letters';
          }
     }
     
     /**
     * Digits
     *
     * @param  string    $key           Field name
     * @param  string    $value         Field value
     * @param  bool      $required      If field is required
     * @return                          void or error message
     */
     public function digit($value, $required) {
          if(empty($value) && $required) {
               return 'Please fill in';
          }
          elseif(!preg_match(self::$check['digit'], $value) && !empty($value)) {
               return 'Use a digit';
          }
     }
     
     /**
     * Images
     *
     * @param  string    $key           Field name
     * @param  string    $value         Field value
     * @param  bool      $required      If field is required
     * @return                          void or error message
     */
     public function image() {
          if(in_array($_FILES['img']['type'], self::$check['image']['types'])) {
               if($_FILES['img']['size']>=self::$check['image']['size']) {
                    return true;
               }
               else {
                    return 'Image is too big';
               }
          }
          else {
               return 'Invalid filetype';
          }
     }
     
     /**
     * Passwords
     *
     * @param  string    $key           Field name
     * @param  string    $value         Field value
     * @param  bool      $required      If field is required
     * @return                          void or error message
     */
     public function password($value, $required) {
          if(empty($value) && $required) {
               return 'Please fill in';
          }
          elseif(!preg_match(self::$check['password'], $value) && !empty($value)) {
               return 'Must be between 6 and 12';
          }
     }
     
     /**
     * Emails
     *
     * @param  string    $key           Field name
     * @param  string    $value         Field value
     * @param  bool      $required      If field is required
     * @return                          void or error message
     */
     public function email($value, $required) {
          if(empty($value) && $required) {
               return 'Please fill in';
          }
          elseif(!preg_match(self::$check['email'], $value) && !empty($value)) {
               return 'Invalid email';
          }
     }

     /**
     * Domain
     *
     * @param  string    $key           Field name
     * @param  string    $value         Field value
     * @param  bool      $required      If field is required
     * @return                          void or error message
     */
     public function domain($value, $required) {
          if(empty($value) && $required) {
               return 'Please fill in';
          } elseif(!preg_match(self::$check['domain'], $value) && !empty($value)) {
               return 'Invalid domain name (Dont use www, but use a top-level-domain)';
          }
     }
     
}

?>

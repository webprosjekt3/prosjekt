<?php
/**
 * Runs shell commands on the server
 */
class Shell {

     function __construct() {
     }

     /**
      * Copy file / folder
      *
      * @param $source        string    Folder or file to copy
      * @param $destination   string    Where to copy to
      * @return                         void
      **/
     public function copy($source, $destination) {
          if(is_dir($source)) $source = "-R $source";
          return shell_exec("cp $source $destination");
     }

     /**
      * Delete file / folder
      *
      * @param $target        string    File or folder to delete
      * @return                         void
      **/
     public function delete($target) {
          if(is_dir($target)) $target = "-rf $target";
          return shell_exec("rm $target");
     }

     /**
      * Control apache 
      * In order to execute the apache2 binary we need permissions to do so with our user. A user has to be added to the server's sudoers file in order to have these permissions
      *
      * @param $action        string    Which action to invoke (start|stop|restart|status|graceful) 
      * @return               string    Shell output 
      **/
     public function apache($action) {
          return shell_exec("sudo ".APACHE_EXEC." $action");
     }

     /**
      * Enable an apache VHost
      * In order to execute the a2ensite binary we need permissions to do so with our user. A user has to be added to the server's sudoers file in order to have these permissions
      *
      * @param $cfg        string    Config file to site to enable 
      * @return            string    Shell output 
      **/
     public function a2ensite($cfg) {
          return shell_exec("sudo ".A2ENSITE_EXEC." $cfg");
     }

     /**
      * Disable an apache VHost
      * In order to execute the a2dissite binary we need permissions to do so with our user. A user has to be added to the server's sudoers file in order to have these permissions
      *
      * @param $cfg        string    Config file to site to disable 
      * @return            string    Shell output 
      **/
     public function a2dissite($cfg) {
          return shell_exec("sudo ".A2DISSITE_EXEC." $cfg");
     }

     /**
      * Control git 
      *
      * @param $target        string    Target folder 
      * @param $action        string    Which action to invoke (pull|commit|add) 
      * @return               string    Shell output 
      **/
     public function git($target, $action) {
          return shell_exec("cd $target && git $action");
     }

     /**
      * Curl 
      *
      * @param $url           string    Url for the request 
      * @param $user          string    A username (optional) 
      * @param $password      string    The password (optional) 
      * @param $request       string    Request method (get|post|put|delete|patch...) 
      * @param $data          string    Request data (Needs to be serialized)
      * @return               string    Shell output 
      **/
     public function curl($url, $user = false, $password = false, $request = false, $data = false) {
          if($user) $user = "-u $user:$password";
          if($request) $request = "-X $request";
          if($data) $data = "-d $data";
          return shell_exec("curl $user $url $request $data");
     }

}
?>

<?php
/**
 * Run git commands both locally and remotely
 */
class Git {
     protected $shell; 
     protected $path; 

     function __construct($path = '') {
          $this->shell = new Shell();
          $this->path = $path;
     }

     /**
      * Create a new repository on github. Sets up the local repository and uses curl to login and creates git files
      *
      * @return void
      */
     public function create($name) {
          $this->shell->git($this->path, 'init');
          $this->shell->git($this->path, 'remote add origin "git@github.com:'.GIT_USER.'/'.$name.'.git"');
          
          $url = GIT_URL.'/user/repos';
          $this->shell->curl($url, GIT_USER, GIT_PASSWORD, 'POST', '"{\"name\":\"'.$name.'\"}"');
     }

     /**
      * Clone a repository
      *
      * @param $url string    Link to repository
      * @return void
      */
     public function cloneByUrl($url) {
          $this->git($this->path, "clone $url");
     }

     /**
      * Delete repository remotely
      *
      * @param $name     string    Name of repository
      * @return void 
      */
     public function delete($name) {
          $url = GIT_URL."/repos/".GIT_USER."/$name";
          $this->shell->curl($url, GIT_USER, GIT_PASSWORD, 'DELETE');
     }

     /**
      * Add files and commit
      *
      * @param $message  string    Commit message
      * @return Shell output
      */
     public function commit($message) {
          $this->shell->git($this->path, 'add . -A');
          return $this->shell->git($this->path, "commit -m $message");
     }

     /**
      * Push commit(s)
      *
      * @return Shell output
      */
     public function push() {
          return $this->shell->git($this->path, 'push -u origin master');
     }

     /**
      * Pull
      *
      * @return Shell output
      */
     public function pull() {
          return $this->shell->git($this->path, 'pull');
     }
     
     /**
      * Update the git status by running git status -s, then remove all the junk we dont need from the output
      *
      * @param $path     string    Path to folder
      * @return The status code
      */
     public function getStatus() {
          $status = current(explode(' ', $this->shell->git($this->path, 'status -s')));
          if(empty($status) || $status === '??') $status = 'OK';
          return $status;
     }

     /**
      * Get remote commits
      *
      * @param $name     string    Name of repository
      * @return commits
      */
     public function getCommitsPrevious($name) {
          $url = GIT_URL."/repos/".GIT_USER."/$name/commits";
          $log = json_decode($this->shell->curl($url, GIT_USER, GIT_PASSWORD), true);
          if(isset($log['message']) && $log['message'] == 'Git Repository is empty.') {
               return false;
          } else {
               return $log;
          }
     }

     /**
      * Gets pending commits by running log, which we will format as a json object, then return the decoded array
      *
      * @param $folder   string    Path to folder
      * @return commits  array
      **/
     public function getCommitsPending() {
          $log = $this->shell->git($this->path, 'log --pretty=format:"{\"sha\":\"%H\",\"committer\":{\"name\":\"%cn\",\"email\":\"%ce\"},\"date\":\"%cd\",\"message\":\"%s\"}," --branches --not --remotes');
          $log = '['.rtrim($log, ',').']';
          return json_decode($log, true);
     }

}
?>

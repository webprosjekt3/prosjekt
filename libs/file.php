<?php
/**
 * File handler class
 */
class File {
     protected $shell;
     public $path;
     public $name;
     public $size;
     public $error;
     public $file;
     public $filePath;
     public $buffer;

     /**
      * Set path to folder
      */
     function __construct($path) {
          $this->path = $path;
          $this->shell = new Shell();
     }

     /**
      * Gets files from the path
      *
      * @param $folder   string    Folder to get from 
      * @return          array     Files with path or false
      */
     public function getFiles($folder) {
          $path = $this->path.$folder;
          if(count(glob($path)) !== 0) {
               $files = array();
               $size = 0;
               $directory = new RecursiveDirectoryIterator($path, FilesystemIterator::CURRENT_AS_SELF);
               $iterator = new RecursiveIteratorIterator($directory);
     
               foreach($iterator as $name => $file) {
                    if($file->isDot()) continue;
                    $files[] = $name;
                    $size += $file->getSize();
               }
     
               $this->setSize($size);
               return $files;
          } else {
               $this->error = 'no file(s) were found';
               return false;
          }
     }
     
     /**
      * Archive a folder or file into a zip archive
      *
      * @param $name     string    Name of archive
      * @return void
      */
     public function archive($name) {
          $zip = new ZipArchive();

          if($zip->open($this->path.$name.'.zip', ZipArchive::OVERWRITE)) {
               $files = $this->getFiles($name);
               if($files) foreach($files as $file) $zip->addFile($file);
               if(!$zip->status == ZipArchive::ER_OK) $this->error = "Can't write files";
               $zip->close();
          } else {
               $this->error = "Can't create archive";
          }

          $this->name = $this->path.$name.'.zip';
     }

     /**
      * Set the size
      *
      * @param      int       Size
      * @return     string    Size in proper measurement
      */
     public function setSize($size) {
          $units = explode(' ', 'B KB MB GB TB PB');
          for($i = 0; $size > 1024; $i++) $size /= 1024;
          $this->size = round($size, 2) .' '. $units[$i];
     }

     /**
      * Make a folder
      *
      * @param $path      string    Path of folder 
      * @return                     void
      **/
     public function mkdir($name) {
          if(!mkdir($this->path.$name)) $this->error = 'Unable to make directory';
     }

     /**
      * Copy file / folder
      * (If value was returned it means an error has most likely occured)
      *
      * @param $source        string    The file / folder to copy 
      * @param $destination   string    Path to copy to
      * @return                         void 
      **/
     public function copy($source, $destination) {
          if($this->shell->copy($this->path.$source, $this->path.$destination)) $this->error = 'Unable to copy';
     }

     /**
      * Rename file / folder
      *
      * @param $target        string    The file / folder to rename
      * @param $new           string    New name
      * @return                         void 
      **/
     public function rename($target, $new) {
          if(!rename($this->path.'/'.$target, $this->path.'/'.$new)) $this->error = 'Unable to rename';
     }

     /**
      * Delete file / folder
      *
      * @param $target        string    The file / folder to delete 
      * @return                         void 
      **/
     public function delete($target) {
          $this->shell->delete($this->path.'/'.$target);
     }

     /**
      * Open and set the file
      * 
      * @param $target   string    Filename
      * @param $mode     string    Mode
      * @return void
      */
     public function openFile($target, $mode) {
          $this->filePath = $this->path.'/'.$target;
          $file = fopen($this->filePath, $mode);
          if($file) {
               $this->file = $file;
          } else {
               $this->error = 'Unable to open file';
          }
     }

     /**
      * Close and unset the file
      *
      * @return void
      */
     public function closeFile() {
          fclose($this->file);
          $this->file = null;
          $this->filePath = null;
     }

     /**
      * File buffer
      * Opens a file and buffers it
      */
     public function bufferFile($file) {
          $this->openFile($file, 'r');
          if($this->file) {
               $this->buffer = fread($this->file, filesize($this->filePath));
               $this->closeFile();
          } else {
               $this->error = 'File not defined';
          }
     }

     /**
      * Append string to file
      *
      * @param $file     string    File to write to
      * @param $string   string    String to append
      * @return void
      */
     public function appendToFile($file, $string) {
          $this->openFile($file, 'a+');
          fwrite($this->file, $string);
          $this->closeFile();
     }

     /**
      * Overwrite content of file
      *
      * @param $file     string    Filename
      * @param $content  string    Content to write
      */
     public function writeFile($file, $content) {
          $this->openFile($file, 'w');
          if($this->file) {
               fwrite($this->file, $content);
               $this->closeFile();
          }
     }

}
?>

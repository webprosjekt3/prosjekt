<?php
class autoload {

     public static function models($model) {
          $file = preg_split('/(?=[A-Z])/', $model);
          $path = './models/'. $file[1] .'.php';
          if(!file_exists($path)) return false;
          require_once $path;
     }

     public static function controllers($controller) {
          $file = preg_split('/(?=[A-Z])/', $controller);
          $path = './controllers/'. strtolower($file[1]) .'.php';
          if(!file_exists($path)) return false;
          require_once $path;
     }

     public static function libraries($library) {
          $path = './libs/'. strtolower($library) .'.php';
          if(!file_exists($path)) return false;
          require_once $path;
     }

}
?>

<?php
class Database extends PDO {

     /**
      * Get parent (PDO) and connect to the database
      */
     function __construct($type, $host, $name, $user, $password) {
          parent::__construct($type.':host='.$host.';dbname='.$name, $user, $password);
     }

     /**
     * Selects values from the database 
     *
     * @param  string              $sql                SQL code
     * @param  string              $mode               Single or multiple values
     * @param  array               $array              Values to be bound
     * @param  constant       $fetch_mode              Which format the results should be in
     * @return                                         Database results
     */
     public function select($sql, $mode, $array = array(), $fetch_mode = PDO::FETCH_ASSOC) {
          $sth = $this->prepare($sql);
          
          foreach ($array as $key => $value) {
               $sth->bindValue(":$key", $value);
          }
          $sth->execute();
          
          if($mode=='single') {
               return $sth->fetch($fetch_mode);
          } elseif($mode=='multi') {
               return $sth->fetchAll($fetch_mode);
          }
     }
     
     /**
     * Inserts values into table
     *
     * @param  string         $table         Which table to be insert into
     * @param  array          $data          Values to be inserted
     */
     public function insert($table, $data) {
          ksort($data);
          
          $fields = implode(',', array_keys($data));
          $values = implode('","', array_values($data));
          
          $sql = 'INSERT INTO '.$table.'('.$fields.') VALUES ("'.$values.'")';
          $sth = $this->prepare($sql);
          
          foreach ($data as $key => $value) {
               $sth->bindValue(":$key", $value);
          }
          
          $sth->execute();
          return true;
     }
     
     /**
     * Updates a table with data
     *
     * @param  string         $table         Which table to be insert into
     * @param  array          $data          Values to be inserted
     * @param  string         $where         Where values needs to be inserted
     * @return                               Database results
     */
     public function update($table, $data, $where) {
          ksort($data);
          $field_details = NULL;
          
          foreach($data as $key=> $value) {
               $field_details .= "`$key`=:$key,";
          }
          
          $field_details = rtrim($field_details, ',');
          $sth = $this->prepare("UPDATE $table SET $field_details WHERE $where");
          
          foreach ($data as $key => $value) {
               $sth->bindValue(":$key", $value);
          }
          $sth->execute();
          return $sth;
     }
     
     /**
     * Deletes values from a table
     *
     * @param  string         $table         Table to delete from
     * @param  array          $where         Value to be deleted
     * @param  int            $limit         Make sure to only delete only x number of times
     * @return                               Database results
     */
     public function delete($table, $where, $limit = 1) {
          return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
     }

}
?>

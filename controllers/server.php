<?php
/**
 * Controller for server actions
 */
class ServerController extends MainController {
     protected $shell;

     /**
      * Get parent and load shell
      */
     function __construct() {
          parent::__construct();
          $this->shell = new Shell();
     }

     /**
      * Start the server
      */
     public function startServer() {
          $this->data['server'] = $this->shell->apache('start');
          $this->setResponse($this->data, 200);
     }

     /**
      * Stop the server
      */
     public function stopServer() {
          $this->data['server'] = $this->shell->apache('stop');
          $this->setResponse($this->data, 200);
     }

     /**
      * Restart the server
      */
     public function restartServer() {
          $this->data['server'] = $this->shell->apache('restart');
          $this->setResponse($this->data, 200);
     }

     /**
      * Reload the servers configuration
      */
     public function reloadServer() {
          $this->data['server'] = $this->shell->apache('reload');
          $this->setResponse($this->data, 200);
     }

     /**
      * Get the server runtime status
      */
     public function statusServer() {
          $this->data['server'] = $this->shell->apache('status');
          $this->setResponse($this->data, 200);
     }

}
?>

<?php
/**
 * Controller for site pages
 */
class SiteController extends MainController {

     /**
      * Gets parent and loads the Site model
      */
     function __construct() {
          parent::__construct();
          $this->SiteModel = new SiteModel();
     }
     
     /**
      * Gets all sites by getting data from model, then render the view with the data and a header and footer
      *
      * @return void
      */
     public function getSitesAll() {
          $sites = $this->SiteModel->selectAll();
          foreach($sites as $site) {
               $this->SiteModel->updateGitStatus($site['id'], $site['domain']);
          }

          $this->data['sites'] = $this->SiteModel->selectAll();
          $this->setResponse($this->data, 200);
     }

     /**
      * Get a site by id from the model, then render the view with the data and a header and footer
      *
      * @param $id  int  Site id
      * @return void
      */
     public function getSiteById($id) {
          $site = $this->SiteModel->selectById($id, 'name, domain');
          $this->SiteModel->updateGitStatus($id, $site['domain']); 

          $site = $this->SiteModel->selectById($id);

          if($site) {
               $this->data['site'] = $site;
               $status = 200;
          } else {
               $status = 404;
          }

          $this->setResponse($this->data, $status);
     }

     /**
      * Get physical files for a site
      *
      * @param	id	int	  Site id
      * @return void
      */
     public function getFilesSite($id) {
          $site = $this->SiteModel->selectById($id);

          if($site) { 
          	$file = new File('sites/');
          	$this->data['files'] = $file->getFiles($site['domain']);
               $status = 200;
          } else {
               $status = 404;
          }

          $this->setResponse($this->data, $status);
     }

     /**
      * Download a site by id. Will archive it, redirect the user to that archive, then delete the archive 
      *
      * @param $id  int  Site id
      * @return void
      */
     public function downloadSite($id) {
          $site = $this->SiteModel->selectById($id, 'domain');

          $file = new File('sites/');
          $file->archive($site['domain']);

          if($file->error) {
               $this->data['errors']['file'] = $file->error;
               $this->setResponse($this->data, 400);
          } else {
               $this->data['file'] = basename($file->name);
               $this->setResponse($this->data, 200, 'file');
               readfile($file->name);
          }

     }

     /**
      * Add a new repository for a site
      *
      * @param $id  int  Site id
      * @return void
      */
     public function gitCreateSite($id) {
          $site = $this->SiteModel->selectById($id, 'name, domain');
          
          $git = new Git('sites/'.$site['domain']);
          $git->create($site['name']);

          $this->SiteModel->updateById($id, array('repo' => GIT_URL."/".GIT_USER."/".$site['name']));

          $this->setResponse($this->data, 200);
     }

     /**
      * Clone a repository to site. 
      * The last . is to clone it into the site directory, else it would create a new directory with the cloned repository's name
      *
      * @param $id  int  Site id
      * @return void
      */
     public function gitCloneSite($id) {
          $request = $this->getRequest();
          $site = $this->SiteModel->selectById($id, 'domain');

          $git = new Git('sites/'.$site['domain']);
          $git->cloneByUrl($request['repositoryUrl']);

          $this->SiteModel->updateById($id, array('repo' => GIT_URL."/".GIT_USER."/".$site['name']));

          $this->setResponse($this->data, 200);
     }

     /**
      * Get commits for sites, both pending and previous
      *
      * @param	id	int	  Site id
      * @return void
      */
     public function gitGetCommitsSite($id) {
          $site = $this->SiteModel->selectById($id);

          if($site) {
          	$git = new Git('sites/'.$site['domain']);
          	$this->data['commits']['previous'] = $git->getCommitsPrevious($site['name']);
               $this->data['commits']['pending'] = $git->getCommitsPending();
               $status = 200;
          } else {
               $status = 404;
          }

          $this->setResponse($this->data, $status);
     }

     /**
      * Commit changes to repository
      *
      * @param $id  int  Site id
      * @return void
      */
     public function gitCommitSite($id) {
          $request = $this->getRequest();
          $site = $this->SiteModel->selectById($id, 'domain');

          $this->validate('message', $request['message'], 'string', true);

          if(empty($this->data['errors'])) {
               $git = new Git('sites/'.$site['domain']);
               $commit = $git->commit($request['message']);

               if(strpos($commit, 'nothing to commit')) {
                    $this->data['errors']['commit'] = $commit;
                    $status = 400;
               } else {
                    $status = 200;
               }

          } else {
               $status = 400;
          }

          $this->setResponse($this->data, $status);
     }

     /**
      * Push site to repository
      *
      * @param $id  int  Site id
      * @return void
      */
     public function gitPushSite($id) {
          $site = $this->SiteModel->selectById($id, 'domain');

          $git = new Git('sites/'.$site['domain']);
          $push = $git->push();

          if(strpos($push, 'Everything up-to-date')) {
               $this->data['errors']['push'] = $push;
               $status = 400;
          } else {
               $status = 200;
          }

          $this->setResponse($this->data, $status);
     }

     /**
      * Pull from repository
      *
      * @return void
      */
     public function gitPullSite($id) {
          $site = $this->SiteModel->selectById($id, 'domain');

          $git = new Git('sites/'.$site['domain']);
          $git->pull();

          $this->setResponse($this->data, 200);
     }

     /**
      * Insert the new site to the database, copies silverstripe template and renames the copied folder to the domain name
      *
      * @param $name  string  Name of site
      * @return void
      */
     public function postNewSite() {
          $request = $this->getRequest();

          $this->validate('name', $request['name'], 'string', true);
          $this->validate('domain', $request['domain'], 'domain', true);

          if(empty($this->data['errors'])) {
               $file = new File('sites/');

               if(isset($request['template'])) {
                    $file->copy('template', $request['domain'], true);
               } else {
                    $file->mkdir($request['domain']);
               }
               
               unset($request['template']);
               $this->data['site'] = $this->SiteModel->insertNew($request);

               $vhost = $this->SiteModel->addVHost($request['domain']);
               if(!$vhost) {
                    $this->data['errors']['VHost'] = $vhost;
                    $status = 400;
               }

               if($file->error) {
               	$this->data['errors']['file'] = $file->error;
                    $status = 400;
               } else {
                    $status = 200;
               }
          } else {
               $status = 400;
          }

          $this->setResponse($this->data, $status);
     }

     /**
      * PUT (edit) a site by getting the html form data (the request), validates fields with values as a datatype and if it's required or not
      * If there were no errors while validating, run the query with site id and request data, then redirect to site view
      * If there was an error, simple get the view again (Because we set the errors to a variable accessable to all controllers, we don't need to pass it) 
      *
      * @param $id  int  Site id
      * @return void
      */
     public function putSiteById($id) {
          $request = $this->getRequest();

          $this->validate('name', $request['name'], 'string', true);
          $this->validate('domain', $request['domain'], 'domain', true);

          if(empty($this->data['errors'])) {
               $status = 200;
               $site = $this->SiteModel->selectById($id, 'domain');

			if($site) {
                    if($site['domain'] !== $request['domain']) {
                         $file = new File('sites');
                         $file->rename($site['domain'], $request['domain']);

                         if($file->error) {
                          	$this->data['errors']['file'] = $file->error;
                              $status = 400;
                  	     }

                         $vhost = $this->SiteModel->editVHost($site['domain'], $request['domain']);
                         if(!$vhost) {
                              $this->data['errors']['vhost'] = $vhost;
                              $status = 400;
                         }
                  	}
                    $this->SiteModel->updateById($id, $request); 
			} else {
			     $status = 404;
			}
          } else {
               $status = 400;
          }

          $this->setResponse($this->data, $status);
     }

     /**
      * Delete a site by removing records from database, physical folder, and remote repository if there is one
      * 
      * @param $id  Site id
      * @return void
      */
     public function deleteSiteById($id) {
          $site = $this->SiteModel->selectById($id, 'name, domain, repo');
          
          if($site) {
               if($site['repo']) {
                    $git = new Git();
                    $git->delete($site['name']);
               }

               $file = new File('sites');
               $file->delete($site['domain'], true);
               $this->SiteModel->deleteById($id);

               $vhost = $this->SiteModel->deleteVHost($site['domain']);
               if(!$vhost) {
                    $this->data['errors']['vhost'] = $vhost;
                    $status = 400;
               }

               if($file->error) {
                    $this->data['errors']['file'] = $file->error;
                    $status = 400;
               } else {
                    $status = 200;
               }
          } else {
               $status = 404;
          }

          $this->setResponse($this->data, $status);
     }
     
}
?>

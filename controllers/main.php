<?php

/**
 * Main controller contains functionality sub-controllers needs access to
 *
 * @var $app   object    The instance of Slim
 * @var $data  array     Data for view
 * @var $shell object    The instance of Shell
 */
abstract class MainController {
     protected $app;
     protected $data;
     protected $shell;

     /**
      * Sets the current instance of Slim
      * Instantiate Shell
      */
     function __construct() {
          $this->app = Slim\Slim::getInstance();
          $this->shell = new Shell();
          $this->data = array();
     }

     /**
      * Load a model by requiring the file and instantiate it, and also make sure we don't include the class twice
      *
      * @return void
      */
     protected function loadModel($model) {
          $modelName = $model .'Model';
          if(!class_exists($modelName)) require_once './models/'. $model .'.php';
          $this->$modelName = new $modelName;
     }

     /**
      * Get request data, which is formated as raw HTML data, parse it into an array which holds fields as keys and values as values.
      * Then remove _METHOD which is only being used by Slim to use the right routing request, so no longer needed
      *
      * @return $data array The HTML form data
      */
     public function getRequest() {
          parse_str($this->app->request->getBody(), $data);
          return $data;
     }

     /**
      * Sends back a response
      *
      * @param $data     array     Data to respond with 
      * @param $status   int       Status code
      * @param $type     string    Type of content, defaults to json
      * @return void
      */
     public function setResponse($data, $status, $type = 'json') {
          $body = true;

          switch($type) {
               case 'json':
                    $type = 'application/json';
                    $data = json_encode($data);
                    break;
               case 'file':
                    $body = false;
                    $type = 'application/octet-stream';
                    $this->app->response->header('Content-Description', 'File Transfer');
                    $this->app->response->header('Content-Disposition', 'attachment; filename='. $data['file']);
                    $this->app->response->header('Content-Transfer-Encoding', 'binary');
                    $this->app->response->header('Expires', '0');
                    $this->app->response->header('Cache-Control', 'must-revalidate');
                    $this->app->response->header('Pragma', 'public');
                    break;
          }

          $this->app->response->header('Content-Type', $type);
          $this->app->response->header('Access-Control-Allow-Origin', '*');
          if($body) $this->app->response->body($data); 
          $this->app->response->status($status);
     }

     /**
      * Validates values for a data type. If there was an error, get the error messages and add them to data view
      *
      * @param $field    string    The field
      * @param $value    string    The value
      * @param $type     string    Type of value
      * @param $required boolean   If field is required
      * @param $args     array     Arguments for data type (ie length should be min x and max y) (optional)
      * @return void
      */
     public function validate($field, $value, $type, $required = false, $args = null) {
          $validate = new Validate();
          $error = $validate->{$type}($value, $required, $args);

          if($error) {
               $this->data['errors'][$field] = $error;
               $this->data['errors']['validate'] = 'There was a validation error!';
          }
     }

}
?>

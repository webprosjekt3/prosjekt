<?php
	$app->options('/(:name+)', function() use ($app) {
		$app->response()->header('Access-Control-Allow-Origin', '*');
    		$app->response()->header('Access-Control-Allow-Headers', 'X-Requested-With, X-authentication, X-client');
    		$app->response()->header('Access-Control-Allow-Methods', 'PUT, GET, DELETE, POST');
	});
?>

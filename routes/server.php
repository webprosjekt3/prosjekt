<?php
     $app->ServerController = new ServerController();
     
     $app->get('/api/server/start', function() use($app) {
          $app->ServerController->startServer();
     });

     $app->get('/api/server/stop', function() use($app) {
          $app->ServerController->stopServer();
     });

     $app->get('/api/server/restart', function() use($app) {
          $app->ServerController->restartServer();
     });

     $app->get('/api/server/reload', function() use($app) {
          $app->ServerController->reloadServer();
     });

     $app->get('/api/server/status', function() use($app) {
          $app->ServerController->statusServer();
     });
?>

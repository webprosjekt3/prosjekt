<?php
     $app->SiteController = new SiteController();
     
     $app->get('/api/sites', function() use($app) {
          $app->SiteController->getSitesAll();
     });

     $app->get('/api/site/:id', function($id) use($app) {
          $app->SiteController->getSiteById($id);
     });
     $app->get('/api/site/:id/files', function($id) use($app) {
          $app->SiteController->getFilesSite($id);
     });
     /*
     $app->get('/api/site/:id/download', function($id) use($app) {
          $app->SiteController->downloadSite($id);
     });
     */

     $app->get('/api/site/:id/git/create', function($id) use($app) {
          $app->SiteController->gitCreateSite($id);
     });
     $app->post('/api/site/:id/git/clone', function($id) use($app) {
          $app->SiteController->gitCloneSite($id);
     });
     $app->get('/api/site/:id/git/commits', function($id) use($app) {
          $app->SiteController->gitGetCommitsSite($id);
     });
     $app->post('/api/site/:id/git/commit', function($id) use($app) {
          $app->SiteController->gitCommitSite($id);
     });
     $app->get('/api/site/:id/git/push', function($id) use($app) {
          $app->SiteController->gitPushSite($id);
     });
     $app->get('/api/site/:id/git/pull', function($id) use($app) {
          $app->SiteController->gitPullSite($id);
     });

     $app->post('/api/site', function() use($app) {
          $app->SiteController->postNewSite();
     });

     $app->put('/api/site/:id', function($id) use($app) {
          $app->SiteController->putSiteById($id);
     });

     $app->delete('/api/site/:id', function($id) use($app) {
          $app->SiteController->deleteSiteById($id);
     });
     
     $app->get('/api/site/:id/test', function($id) use ($app) {
     	  $app->SiteController->testSiteById($id);
     });
?>

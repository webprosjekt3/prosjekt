<?php
/**
 * Main model contains functionality sub-models needs access to
 *
 * @var $app   object    The instance of Slim
 * @var $db    object    The database
 */
abstract class MainModel {
     protected $app;
     protected $db;
     protected $shell;
     
     /**
      * Sets the current instance of Slim
      * Loads the database object with connection info
      */
     function __construct() {
          $this->app = Slim\Slim::getInstance();
          $this->loadDatabase(TYPE, HOST, NAME, USER, PASS);
          $this->shell = new Shell();
     }

     /**
      * Load the database by requiring the file and instantiate it, also make sure we don't include the class twice
      */
     protected function loadDatabase($type, $host, $name, $user, $password) {
          $this->db = new Database($type, $host, $name, $user, $password);
     }

}
?>

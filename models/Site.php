<?php
/**
 * Site model handles data about sites
 */
class SiteModel extends MainModel {
     
     /**
      * Get parent
      */
     function __construct() {
          parent::__construct();
     }

     /**
      * Get all sites by running a database sql query
      *
      * @param $columns  string    Columns to select from
      * @return array              Database data
      */
     public function selectAll($columns = false) {
          if(!$columns) $columns = '*';
          return $this->db->select("select $columns from sites", 'multi');
     }

     /**
      * Get a site by id
      
      * Binds values so we can let PDO handle input values for us (:id is the id key in the array)
      * Instead of making new functions every time we want different columns we now just pass them in if we want specific ones
      *
      * @param $id       int       Site id
      * @param $columns  string    Columns to select from
      * @return array              Database data
      */
     public function selectById($id, $columns = false) {
          if(!$columns) $columns = '*';
          return $this->db->select("select $columns from sites where id = :id", 'single', array('id' => $id));
     }

     /**
      * Add a new site
      *
      * @param $data     array     Site values
      * @return array    Database data
      */
     public function insertNew($data) {
          return $this->db->insert('sites', $data);
     }

     /**
      * Edit a site by id
      *
      * @param $id       int       Site id
      * @param $data     array     New values
      * @return array    Database data
      */
     public function updateById($id, $data) {
          return $this->db->update('sites', $data, "id = $id");
     }

     /**
      * Delete a site by id
      *
      * @param $id  int  Site id
      * @return string    Database results
      */
     public function deleteById($id) {
          return $this->db->delete('sites', "id = $id");
     }

     /**
      * Get status and update database
      * 
      * @param $id       int  Site id
      * @param $domain   string    Site domain
      * @return void
      */
     public function updateGitStatus($id, $domain) {
          $git = new Git("sites/$domain");
          $status = $git->getStatus();
          $this->updateById($id, array('status' => $status));
     }

     /**
      * Add site entry to the VHost file by creating a config file and adding entry for its domain
      * Will also reload server configuration
      *
      * @param $folder   string   Site domain 
      * @return bool true or false
      */
     public function addVHost($domain) {
          $sitePath = SITE_ROOT."/sites/$domain";
          $entry = "<VirtualHost *:".VHOST_PORT.">
ServerName $domain
ServerAlias www.$domain
DocumentRoot $sitePath 
<Directory $sitePath>
Allow from all
AllowOverride all
Options -MultiViews 
Options FollowSymLinks
</Directory>
</VirtualHost>";

          $cfg = "$domain.conf";
          $file = new File(VHOST_PATH);
          $file->writeFile("$domain.conf", $entry);

          if($file->error) {
               return $file->error;
          } else {
               $shell = new Shell();
               $shell->a2ensite($cfg);
               $shell->apache('reload');
               return true;
          }
     }

     /**
      * Edit a VHost site entry
      * Gets a buffer from the VHost file and changes lines where a site entry has been hit on its domain, then write the file. This function is dangerous and should have better error checking and error handling
      * Will reload apache configuration after file were written
      *
      * @param $oldDomain     string    Old domain entry
      * @param $newDomain     string    New domain entry
      * @return error message or true
      */
     public function editVHost($oldDomain, $newDomain) {
          $sitePath = SITE_ROOT."/sites/";

          $oldCfg = "$oldDomain.conf";
          $newCfg = "$newDomain.conf";
          $file = new File(VHOST_PATH);
          $file->bufferFile($oldCfg);

          $vhost = explode("\n", $file->buffer);

          if(!empty($vhost)) {
               if(empty($file->error)) {
                    $vhost[1] = "ServerName $newDomain";
                    $vhost[2] = "ServerAlias www.$newDomain";
                    $vhost[3] = "DocumentRoot ".$sitePath."$newDomain";
                    $vhost[4] = "<Directory ".$sitePath."$newDomain>";
          
                    $vhost = implode("\n", $vhost);
                    $file->rename($oldCfg, $newCfg);
                    $file->writeFile($newCfg, $vhost);

                    $shell = new Shell();
                    $shell->a2ensite($newCfg);
                    $shell->a2dissite($oldCfg);

                    $shell->apache('reload');

                    $error = false;
               } else {
                    $error = true;
               }
          } else {
               $error = true;
          }

          if($file->error) {
               return $file->error;
          } elseif($error) {
               return 'Could not retrieve site entry from VHost file!';
          } else {
               return true;
          }
     }

     /**
      * Delete a VHost site entry by removing both the config file and symlinks
      * Then reload server configuration
      *
      * @param $domain   string    Site domain
      * @return error or true
      */
     public function deleteVHost($domain) {
          $cfg = "$domain.conf";

          $shell = new Shell();
          $shell->a2dissite($cfg);
          $shell->apache('reload');

          $file = new File(VHOST_PATH);
          $file->delete($cfg);

          if($file->error) {
               return $file->error;
          } else {
               return true;
          }
     }

} 
?>

<?php 
     require_once './vendor/autoload.php';
     require_once './libs/autoload.php';
     require_once './controllers/main.php';
     require_once './models/main.php';

     spl_autoload_register('autoload::controllers');
     spl_autoload_register('autoload::libraries');
     spl_autoload_register('autoload::models');

     $app = new \Slim\Slim(array(
          'mode' => 'development',
          'debug' => true,
     ));

     // Require all configurations
     foreach(scandir('./config/') as $file) {
          $config = './config/'. $file;
          if(is_file($config)) require $config;
     }

     // Require all routes
     foreach(scandir('./routes/') as $file) {
          $route = './routes/'. $file;
          if(is_file($route)) require $route;
     }

     $app->run();
?>
